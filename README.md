# Prueba de concepto Ceph

El objetivo de esta prueba de concepto es implementar Ceph en un entorno virtualizado de desarrollo, con consideraciones de red similares a las que se utilizarán en producción.

## Requerimientos

* Procesador de 4 núcleos
* 8GB de RAM, preferentemente 12GB o más.

Requiere el uso de VirtualBox y Vagrant. Adicionalmente, son necesarios los siguientes plugins de Vagrant:

* vagrant-hostmanager
* vagrant-cachier

## Configuración

La configuración se realiza en el Vagrantfile.

### Recursos

A continuación se definen los ajustes de recursos para cada nodo. La variable OSD implica la creación de un disco de aprovisionamiento delgado que será creado en el propio directorio del repositorio. 

    RAM = 4096   # Default memory size in MB
    GUI = false # Enable/Disable GUI
    BOX = 'debian/bullseye64'
    OSD = 10240 # Default OSD Size (MBytes)

### Redes

Se definen dos redes. La red pública donde se prestarán los servicios y la red de replicación de los OSD (cluster_network).
Estas redes deben definirse en el archivo Vagrantfile. Por ejemplo:

    DOMAIN = "ceph.local"
    PUBLIC_NETWORK = "192.168.56"
    PUBLIC_NETMASK = "255.255.255.0"
    CLUSTER_NETWORK = "192.168.57"
    CLUSTER_NETMASK = "255.255.255.0"

## Aprovisionamiento

Dentro del directorio 'provision' encontramos los distintos scripts para la configuración de los equipos. El script ceph-common.sh es el general para todos los equipos y ceph-bootstrap es el que inicia el cluster e instala los servicios.

Una vez terminado el aprovisionamiento, tendremos el panel de admisnitración en https://ceph1.ceph.local:8443. El usuario es admin y la clave aleatoria se mostrará en el log de Vagrant.

## RADOS Gateway

Adicionalmente al servicio de bloques, se instala un servicio de objetos llamado RADOS Gateway. Es servicio es compatible con Amazon S3.

Para realizar pruebas de funcionamiento de la intefaz s3 de ceph, utilizamos https://github.com/jenshadlich/S3-Performance-Test

También es posible utilizar https://github.com/bibby/radula o bien s3cmd.

### Creamos un usuario de pruebas

radosgw-admin user create --uid="radosgw-test" --display-name="RGW Test" | grep -e 'access_key\|secret_key'
 "access_key": "KWQE4EQAWAGG4Z1SO9GE",
 "secret_key": "A0dBtphTdBAcHmpqeU3zv2XtAm33UWbDPxhJypnG"

### Creamos un bucket

    java -jar target/s3pt.jar --endpointUrl ceph1.ceph.local \
        --accessKey KWQE4EQAWAGG4Z1SO9GE  \
        --secretKey A0dBtphTdBAcHmpqeU3zv2XtAm33UWbDPxhJypnG \
        --bucketName pruebas --operation=CREATE_BUCKET -n 1 --usePathStyleAccess

### Pruebas de escritura

    java -jar target/s3pt.jar --endpointUrl ceph1.ceph.local \
        --accessKey KWQE4EQAWAGG4Z1SO9GE  \
        --secretKey A0dBtphTdBAcHmpqeU3zv2XtAm33UWbDPxhJypnG \
        --bucketName pruebas --operation=UPLOAD_AND_READ -n 10 -t 32 --usePathStyleAccess

Es posible realizar las siguientes operaciones:

    CLEAR_BUCKET
    CREATE_BUCKET
    CREATE_KEY_FILE
    RANDOM_READ
    RANDOM_READ_METADATA
    UPLOAD_AND_READ
    UPLOAD
