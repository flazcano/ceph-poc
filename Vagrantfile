# -*- mode: ruby -*-
# vim: ft=ruby

# ---- Configuración: variables ---- #
RAM = 2048   # Default memory size in MB
GUI = false # Enable/Disable GUI
BOX = 'debian/bookworm64'
OSD = 10240 # Default OSD Size (MBytes)
# Network configuration
DOMAIN = "ceph.local"
PUBLIC_NETWORK = "192.168.56"
PUBLIC_NETMASK = "255.255.255.0"
CLUSTER_NETWORK = "192.168.57"
CLUSTER_NETMASK = "255.255.255.0"

CEPH_RELEASE = "squid"

HOSTS = {
# "hostname" => ["IP", RAM, GUI, BOX, OSD_DISK_SIZE], 
# Ceph1 debe ser el último hosts, para respetar el orden de provisión.
   "ceph3" => ["103", RAM, GUI, BOX, OSD],
   "ceph2" => ["102", RAM, GUI, BOX, OSD],
   "ceph1" => ["101", RAM, GUI, BOX, OSD]
}

# --- Fin de configuración --- #


# ---- Vagrant ---- #

Vagrant.configure(2) do |config|

  # Tomado desde https://github.com/CesarBallardini/pve6-lab/
  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true

    # uso cachier con NFS solamente si el hostmanager gestiona los nombres en /etc/hosts del host
    if Vagrant.has_plugin?("vagrant-cachier")
      config.cache.auto_detect = false
      config.cache.synced_folder_opts = { owner: "_apt" }
      config.cache.scope = :box
   end
  end
  
  HOSTS.each do | (name, cfg) |
    ipaddr, ram, gui, box, osd = cfg

    config.vm.define name do |machine|
      machine.vm.box   = box
      machine.vm.guest = :debian
      machine.vm.provider "virtualbox" do |vbox|
        vbox.gui = gui
        vbox.memory = ram
        vbox.name = name

        unless File.exist?("#{name}-osd-disk.vdi")
          vbox.customize ['createhd',
                             '--filename', "#{name}-osd-disk",
                             '--size', "#{osd}"]
          vbox.customize ['storageattach', :id,
                             '--storagectl', 'SATA Controller',
                             '--port', 1,
                             '--device', 0,
                             '--type', 'hdd',
                             '--medium', "#{name}-osd-disk.vdi"]
        end
      end

      machine.vm.synced_folder '.', '/vagrant', disabled: true
      machine.vm.hostname = name + '.' + DOMAIN
      machine.vm.network 'private_network', ip: PUBLIC_NETWORK + '.' + ipaddr, netmask: PUBLIC_NETMASK
      machine.vm.network 'private_network', ip: CLUSTER_NETWORK + '.' + ipaddr, netmask: CLUSTER_NETMASK
      
      # Agregamos también los nombres cortos a /etc/hosts
      if Vagrant.has_plugin?("vagrant-hostmanager")
        machine.hostmanager.aliases = %W(#{name})
      end
      
      if name == "ceph1"
        machine.vm.provision "ssh_priv_key", type: "file", source: "provision/ceph-poc.private.key", destination: "/tmp/id_rsa"
        machine.vm.provision "ceph-bootstrap", type: "shell", path: "provision/ceph-bootstrap.sh"
      end
             
    end # config.vm

  end # HOSTS-each
  
  # Aprovisionamiento común
  config.vm.provision "Vagrantfile", type: "file", source: "Vagrantfile", destination: "/tmp/Vagrantfile"
  config.vm.provision "ssh_pub_key", type: :shell do |s|
    begin
      ssh_pub_key = File.readlines("provision/ceph-poc.public.key").first.strip
      s.inline = <<-SHELL
            mkdir -p /root/.ssh/
            touch /root/.ssh/authorized_keys
            echo #{ssh_pub_key} >> /root/.ssh/authorized_keys
            echo #{ssh_pub_key} >> /root/.ssh/id_rsa.pub
      SHELL
    rescue
        puts "Llave pública no encontrada"
        s.inline = "echo OK sin claves publicas"
    end
  end
  
  config.vm.provision "fix-no-tty", type: "shell" do |s|
        s.privileged = false
        s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end

  config.vm.provision "ceph-common", type: "shell", path: "provision/ceph-common.sh"

end
