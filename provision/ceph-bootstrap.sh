#!/usr/bin/env bash

# Variables
DEBIAN_FRONTEND=noninteractive
CEPH_PUBLIC_NETWORK=$(grep PUBLIC_NETWORK /tmp/Vagrantfile | grep -Eo '([0-9]{1,3}[\.]*){4}')".0/24"
CEPH_CLUSTER_NETWORK=$(grep CLUSTER_NETWORK /tmp/Vagrantfile | grep -Eo '([0-9]{1,3}[\.]*){4}')".0/24"
CEPH_DOMAIN=$(grep "DOMAIN =" /tmp/Vagrantfile | cut -d'"' -f2)
CEPH_RELEASE=$(grep CEPH_RELEASE /tmp/Vagrantfile | cut -d\" -f2)
CEPH1_IP=$(ip addr | grep 'state UP' -A 3 | grep inet | grep ${CEPH_PUBLIC_NETWORK%.0/24} | cut -d'/' -f1 | awk '{print $2}')
OTHER_HOSTS=$(grep ceph /etc/hosts | grep -v 127.0 | grep -v ${CEPH_DOMAIN} | awk '{print $2}' | sort -u | grep -v ceph1 | xargs -d '\n' echo)

# Preparamos las conexiones SSH
mv /tmp/id_rsa /root/.ssh/id_rsa 
chown root:root /root/.ssh/id_rsa
chmod 400 /root/.ssh/id_rsa
touch /root/.ssh/known_hosts
grep ${CEPH_DOMAIN} /etc/hosts | grep -v 127.0 | awk '{print $2}' | sort -u | xargs ssh-keyscan >> /root/.ssh/known_hosts
grep ceph /etc/hosts | grep -v 127.0 | grep -v ${CEPH_DOMAIN} | awk '{print $2}' | sort -u | xargs ssh-keyscan >> /root/.ssh/known_hosts

# Iniciamos el cluster Ceph
apt-get -y install cephadm
cephadm bootstrap --mon-ip ${CEPH1_IP}  --cluster-network ${CEPH_CLUSTER_NETWORK}
cephadm install ceph-common

# Definimos la red de monitores
ceph config set mon public_network ${CEPH_PUBLIC_NETWORK}

# Agregamos los otros hosts al cluster
for server in $OTHER_HOSTS ; do 
  ssh-copy-id -f -i /etc/ceph/ceph.pub root@${server} ;
  ceph orch host add ${server} --labels _admin ;
done

# Usamos todos los discos disponibles como OSD
ceph orch apply osd --all-available-devices

# Creamos una instancia de RADOS GW
ceph orch apply rgw ceph-poc

# Configuramos Grafana
ceph dashboard set-grafana-api-url https://ceph1.ceph.local:3000
ceph dashboard set-grafana-api-ssl-verify False

# Esperamos el estado del cluster
echo "Esperando el estado de salud del cluster..."
CEPH_HEALTH=`/usr/bin/ceph health`
while [[ ${CEPH_HEALTH} != "HEALTH_OK"  ]]; do
        sleep 10;
	echo -n "."
        CEPH_HEALTH=`/usr/bin/ceph health`
done
echo -n "OK"

# Creamos un pool para pruebas
ceph osd pool create ceph-poc
ceph osd pool application enable ceph-poc rbd
# Creamos una imagen para pruebas
rbd create ceph-poc/imagen1 --size 1GB


