#!/usr/bin/env bash

## Tareas comunes para todos los servidores Ceph
CEPH_RELEASE=$(grep CEPH_RELEASE /tmp/Vagrantfile | cut -d\" -f2)
CODENAME=`lsb_release -sc`
DEBIAN_FRONTEND=noninteractive

echo "America/Argentina/Buenos_Aires" >/etc/timezone
ln -fs /usr/share/zoneinfo/`cat /etc/timezone` /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

# Agregamos contrib y non-free
sed -i 's/main/main contrib non-free/g' /etc/apt/sources.list

# update / upgrade
apt-get update 
apt-get -y upgrade

# aplicaciones base y requerimientos de Ceph
apt-get -y install neovim htop iotop sudo apt-transport-https ca-certificates gnupg2 podman lvm2 zstd

# Fuentes de Software para Ceph
wget -q -O- 'http://download.ceph.com/keys/release.asc' | apt-key add -
echo 'deb https://download.ceph.com/debian-'${CEPH_RELEASE}'/ '${CODENAME}' main' > /etc/apt/sources.list.d/ceph-${CEPH_RELEASE}.list
apt-get update
